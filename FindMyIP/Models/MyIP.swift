//
//  MyIP.swift
//  FindMyIP
//
//  Created by Waqas Haider on 10/20/23.
//

import Foundation

public enum LoadingState<Value> {
    case loading
    case failed(Error)
    case loaded(Value)
}

public struct MyIP: Codable {
    let ip: String
    let network: String
    let version: String
    let city: String
    let region: String
    let regionCode: String
    let countryName: String
    let countryCode: String
    let countryCapital: String
    let continentCode: String
    let postalCode: String
    let latitude: Double
    let longitude: Double
    let timezone: String
    let countryCallingCode: String
    let currency: String
    let asn: String
    let organization: String
    
    enum CodingKeys: String, CodingKey {
        case ip
        case network
        case version
        case city
        case region
        case regionCode = "region_code"
        case countryName = "country_name"
        case countryCode = "country_code"
        case countryCapital = "country_capital"
        case continentCode = "continent_code"
        case postalCode = "postal"
        case latitude
        case longitude
        case timezone
        case countryCallingCode = "country_calling_code"
        case currency
        case asn
        case organization = "org"
    }
}
