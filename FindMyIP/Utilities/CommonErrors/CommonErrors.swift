//
//  CommonErrors.swift
//  FindMyIP
//
//  Created by Waqas Haider on 10/20/23.
//

import Foundation

public enum CommonErrors: String, Error {
    case invalidRequest = "Request cannot be completed at this moment, Please try again later."
    case unKnown = "Something went wrong!"
    case noRecords = "No record found!"
    
    public var message: String {
        return rawValue
    }
}
