//
//  MyIPViewModel.swift
//  FindMyIP
//
//  Created by Waqas Haider on 10/20/23.
//

import Foundation
import Combine
import Alamofire

public class MyIPViewModel: ObservableObject {
    @Published public private(set) var loadingState: LoadingState<MyIP>
    
    public init(loadingState: LoadingState<MyIP> = .loading) {
        self.loadingState = loadingState
    }
    
    // contants
    let loadingText = "Loading IP Information..."
    
    @MainActor
    public func loadIPInformation() async {
        // prepare request url
        guard let url = URL(string: "https://ipapi.co/json/") else {
            // update state to error with information
            loadingState = .failed(CommonErrors.invalidRequest)
            return
        }
        
        // load data
        AF.request(url, method: .get)
            .response { [weak self] response in
                switch response.result {
                case .success(let data):
                    do {
                        let myIPInformation = try JSONDecoder().decode(MyIP.self, from: data!)
                        print("my IP information: \(myIPInformation)")
                        
                        // update state to loaded with IP information
                        self?.loadingState = .loaded(myIPInformation)
                    } catch {
                        // update state to error with information
                        self?.loadingState = .failed(CommonErrors.noRecords)
                    }
                case .failure(let error):
                    // update state to error with information
                    self?.loadingState = .failed(error)
                }
            }
    }
}
