//
//  FindMyIPApp.swift
//  FindMyIP
//
//  Created by Waqas Haider on 10/23/23.
//

import SwiftUI

@main
struct FindMyIPApp: App {
    var body: some Scene {
        WindowGroup {
            FindMyIPView()
        }
    }
}
