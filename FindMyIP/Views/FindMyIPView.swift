//
//  ContentView.swift
//  FindMyIPView
//
//  Created by Waqas Haider on 10/20/23.
//

import SwiftUI

public struct FindMyIPView: View {
    @ObservedObject private var viewModel = MyIPViewModel()
    
    public init(viewModel: MyIPViewModel = MyIPViewModel()) {
        self.viewModel = viewModel
    }
    
    public var body: some View {
        VStack {
            switch viewModel.loadingState {
            case .loading:
                LoadingView(title: viewModel.loadingText)
                    .task { await viewModel.loadIPInformation() }
            case .loaded(let myIPDetails):
                NavigationStack {
                    successView(with: myIPDetails)
                        .navigationTitle("IP Details:")
                }
            case .failed(let error):
                if let commonError = error as? CommonErrors {
                    failureView(with: commonError)
                } else {
                    failureView(with: .unKnown)
                }
            }
        }
    }
    
    private func successView(with myIPDetails: MyIP) -> some View {
        List {
            RowView(title: "IP", Value: myIPDetails.ip)
            RowView(title: "Network", Value: myIPDetails.network)
            RowView(title: "Version", Value: myIPDetails.version)
            RowView(title: "ASN", Value: myIPDetails.asn)
            RowView(title: "Organization", Value: myIPDetails.organization)
            RowView(title: "Latitude", Value: "\(myIPDetails.latitude)")
            RowView(title: "Longitude", Value: "\(myIPDetails.longitude)")
            RowView(title: "City", Value: myIPDetails.city)
            RowView(title: "Postal Code", Value: myIPDetails.postalCode)
            RowView(title: "State/Region", Value: myIPDetails.region)
            RowView(title: "Country Name", Value: myIPDetails.countryName)
            RowView(title: "Capital", Value: myIPDetails.countryCapital)
            RowView(title: "Continent Code", Value: myIPDetails.continentCode)
            RowView(title: "Timezone", Value: myIPDetails.timezone)
            RowView(title: "Calling Code", Value: myIPDetails.countryCallingCode)
            RowView(title: "Currency", Value: myIPDetails.currency)
        }
    }
    
    private func failureView(with error: CommonErrors) -> some View {
        Text(error.message)
            .font(.title)
            .padding()
    }
}

struct RowView: View {
    let title: String
    let Value: String
    
    var body: some View {
        HStack(alignment: .top) {
            Text("\(title):")
                .font(.headline)
            Text(Value)
        }
    }
}

#Preview {
    FindMyIPView()
}
