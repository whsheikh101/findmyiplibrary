//
//  LoadingView.swift
//  FindMyIP
//
//  Created by Waqas Haider on 10/20/23.
//

import SwiftUI

struct LoadingView: View {
    var title: String
    
    var body: some View {
        ProgressView()
            .controlSize(.large)
        Text(title)
            .font(.title)
    }
}

#Preview {
    LoadingView(title: "Loading...")
}
