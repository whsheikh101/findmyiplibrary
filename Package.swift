// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FindMyIPPackage",
    platforms: [.macOS(.v13),
                .iOS(.v16)],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "FindMyIP",
            targets: ["FindMyIP"]),
    ],
    dependencies: [
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.8.0"))
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "FindMyIP",
            dependencies: [.product(name: "Alamofire", package: "Alamofire")],
            path: "FindMyIP",
            exclude: ["Assets.xcassets",
                      "FindMyIPApp.swift",
                     "Preview Content"]),
        .testTarget(
            name: "FindMyIPTests",
            dependencies: ["FindMyIP"]),
    ]
)
