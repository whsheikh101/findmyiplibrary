import XCTest
@testable import FindMyIP
import Combine

final class FindMyIPLibraryTests: XCTestCase {
    let timeout = 60.0
    var subscription: AnyCancellable?
    var myIPViewModel: MyIPViewModel?
    
    override func setUpWithError() throws {
        myIPViewModel = MyIPViewModel()
    }
    
    override func tearDownWithError() throws {
        subscription = nil
        myIPViewModel = nil
    }
    
    func testloadIPInformation() async {
        guard let unwrappedViewModel = myIPViewModel else { return }
        
        // expectation message
        let expectation = expectation(description: "Load IP Information")
        
        // call load schools API
        await unwrappedViewModel.loadIPInformation()
        
        // start subscription
        subscription = unwrappedViewModel.$loadingState.sink(receiveCompletion: { _ in }, receiveValue: {[weak self] loadingState in
            switch loadingState {
            case .loaded(_):
                XCTAssert(true)
                expectation.fulfill()
                
                // cancel subscription
                self?.subscription?.cancel()
            case .failed(let error):
                XCTAssert(false, (error as? CommonErrors)?.message ?? "")
                expectation.fulfill()
                
                // cancel subscription
                self?.subscription?.cancel()
            default: break
            }
        })
        
        await fulfillment(of: [expectation], timeout: timeout)
    }
}
